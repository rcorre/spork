package main

import (
	"flag"
	"log"
	"os"
	"os/user"
	"path/filepath"
)

func defaultConfigPath() string {
	usr, err := user.Current()
	if err != nil {
		return "spork.json"
	}

	return filepath.Join(usr.HomeDir, ".config", "spork", "spork.json")
}

func main() {
	configPath := flag.String("config", defaultConfigPath(), "path to config file")
	flag.Parse()
	conf, err := LoadConfig(*configPath)
	if err != nil {
		log.Panicln(err)
	}
	if logFile, err := os.OpenFile(conf.LogFilePath, os.O_WRONLY|os.O_CREATE, 0666); err != nil {
		panic(err)
	} else {
		log.SetOutput(logFile)
	}
	log.Println("Config:", conf)

	token, ok := os.LookupEnv("SPARK_TOKEN")
	if !ok {
		log.Panicln("SPARK_TOKEN must be set")
	}

	spark := NewSpark(conf.SparkURL, token)
	core := NewCore(spark)
	if err := core.LoadRooms(); err != nil {
		log.Panicln(err)
	}

	ui := NewCUI(core)
	ui.Draw()
}

//func listen(s Spark, m Manager, conf *Config) {
//	if err := s.Events.Register(); err != nil {
//		panic(err)
//	}
//	defer func() {
//		if err := s.Events.UnRegister(); err != nil {
//			log.Println("Failed to unregister websocket:", err)
//		} else {
//			log.Println("Device unregistered")
//		}
//	}()
//	msgChan, errChan, err := s.Events.Listen()
//	if err != nil {
//		panic(err)
//	}
//
//	interrupt := make(chan os.Signal, 1)
//	signal.Notify(interrupt, os.Interrupt)
//
//	for {
//		select {
//		case msg := <-msgChan:
//			log.Println("event:", msg)
//		case err := <-errChan:
//			log.Println("event error:", err)
//		case <-interrupt:
//			return
//		}
//	}
//}
