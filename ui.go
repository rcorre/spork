package main

import (
	"fmt"

	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
)

// View is the user interface
type View interface {
	Draw() error
}

type cui struct {
	core  *Core
	app   *tview.Application
	rooms *tview.List
	chat  *tview.TextView
	input *tview.InputField
	grid  *tview.Grid
}

// NewCUI creates a new console ui
func NewCUI(core *Core) View {
	app := tview.NewApplication()
	c := cui{
		core: core,
		app:  app,
	}

	c.chat = tview.NewTextView().
		SetDynamicColors(true).
		SetRegions(true).
		SetChangedFunc(func() {
			app.Draw()
		})

	c.rooms = tview.NewList().
		ShowSecondaryText(false)

	for _, room := range core.Rooms {
		c.rooms.AddItem(room.Title, "", 0, nil)
	}

	c.input = tview.NewInputField().
		SetLabel("> ").
		SetFieldWidth(10).
		SetDoneFunc(func(key tcell.Key) {
		})

	c.grid = tview.NewGrid().
		SetRows(0, 3).
		SetColumns(30, 0).
		SetBorders(true).
		AddItem(c.rooms, 0, 0, 1, 1, 0, 0, false).
		AddItem(c.chat, 0, 1, 1, 1, 0, 0, false).
		AddItem(c.input, 1, 1, 1, 1, 0, 0, false)

	app.SetInputCapture(func(key *tcell.EventKey) *tcell.EventKey {
		if key.Key() == tcell.KeyCtrlJ {
			c.cycleRoom(1)
		} else if key.Key() == tcell.KeyCtrlK {
			c.cycleRoom(-1)
		}
		return key
	})

	app.SetRoot(c.grid, true)
	app.SetFocus(c.input)

	return &c
}

func (c *cui) cycleRoom(dir int) {
	idx := c.core.CycleRoom(dir)
	c.rooms.SetCurrentItem(idx)
	room := c.core.Rooms[idx]
	c.core.LoadMessages(room.ID)
	messages := c.core.Messages[room.ID]
	c.chat.Clear()
	c.drawMessages(messages)
}

func (c *cui) drawMessages(messages []Message) {
	var curSender string
	c.chat.Clear()
	for _, m := range messages {
		if m.PersonID != curSender {
			curSender = m.PersonID
			sender := c.core.People[curSender].DisplayName
			fmt.Fprintf(c.chat, "\n--- %s (%s)  ---\n", sender, m.Created)
		}
		if m.HTML != "" {
			fmt.Fprintln(c.chat, m.Text)
		} else {
			fmt.Fprintln(c.chat, m.Text)
		}
	}
}

func (c *cui) Draw() error {
	err := c.app.Run()
	return err
}
