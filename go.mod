module github.com/rcorre/spork

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gdamore/encoding v0.0.0-20151215212835-b23993cbb635 // indirect
	github.com/gdamore/tcell v1.1.0
	github.com/gopherjs/gopherjs v0.0.0-20181004151105-1babbf986f6f // indirect
	github.com/gorilla/websocket v1.2.0
	github.com/jtolds/gls v4.2.1+incompatible // indirect
	github.com/lucasb-eyer/go-colorful v0.0.0-20180709185858-c7842319cf3a // indirect
	github.com/mattn/go-colorable v0.0.0-20171111065953-6fcc0c1fd9b6 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/mattn/go-runewidth v0.0.3 // indirect
	github.com/mgutz/ansi v0.0.0-20170206155736-9520e82c474b
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rivo/tview v0.0.0-20180926100353-bc39bf8d245d
	github.com/satori/go.uuid v1.1.0
	github.com/smartystreets/assertions v0.0.0-20180927180507-b2de0cb4f26d // indirect
	github.com/smartystreets/goconvey v0.0.0-20180222194500-ef6db91d284a // indirect
	github.com/stretchr/objx v0.0.0-20140526180921-cbeaeb16a013 // indirect
	github.com/stretchr/testify v1.2.2
	golang.org/x/net v0.0.0-20180108090419-434ec0c7fe37
	golang.org/x/sys v0.0.0-20180905080454-ebe1bf3edb33 // indirect
	golang.org/x/text v0.3.0 // indirect
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0 // indirect
)
